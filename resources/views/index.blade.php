<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <title>Products</title>
</head>
<body>
<h1>Products</h1>

<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Price</th>
        <th scope="col">Actions</th>
    </tr>
    </thead>
    <tbody id="products-list">
    <tr>
        <td>


        </td>
    </tr>
    </tbody>
</table>
<div>
    <a href="{{route('products.create-view')}}">
        <button class="btn btn-xs btn-default text-primary mx-1 shadow">
            Create product
        </button>
    </a>
</div>


<script
    src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
    crossorigin="anonymous"
></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(async function () {
        try {
            let products = await $.getJSON('/api/product-list');
            let html = '';
            $.each(products, function (index, product) {
                $.each(product, function (index, row) {
                    html += '<tr>';
                    html += '<td>' + row['id'] + '</td>';
                    html += '<td>' + row['name'] + '</td>';
                    html += '<td>' + row['description'] + '</td>';
                    html += '<td>' + row['price'] + '</td>';
                    html += '<td> <a href="/api/product-edit/' + row['id'] + '"> <button class="btn btn-xs btn-default text-teal mx-1 shadow"> Edit </button> </a> ' +
                        '<button class="btn btn-danger delete-btn" data-id="' + row['id'] + '"> Delete </button>' +
                        '</td>';
                    html += '</tr>';
                });

            });
            $('#products-list').html(html);
        } catch (error) {
            console.error(error);
        }
    });

    $('.delete-btn').click(function() {
        let id = $(this).data('id');
        $.ajax({
            url: '/api/product-delete/' + id,
            type: 'DELETE',
            success: function(response) {
                location.reload();
            }
        });
    });
</script>
</body>
</html>
