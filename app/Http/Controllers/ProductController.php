<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return ProductResource::collection(Product::all());
    }

    public function edit(Product $product)
    {
        return view('edit', compact('product'));
    }

    public function store(CreateProductRequest $request)
    {
        $product = new Product([
            'name' => $request->name,
            'description' => $request->description ?? '',
            'price' => $request->price,
        ]);

        $product->save();

        return redirect(route('products.index'))->with('Success', 'Product successfully created');
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        $product->update([
            'name' => $request->name,
            'description' => $request->description,
            'price' => $request->price
        ]);

        $product->save();

        return redirect(route('products.index'))->with('Success', 'Product successfully updated');
    }

    public function destroy(Product $product)
    {
        $product->delete();

        return redirect(route('products.index'))->with('Success', 'Product successfully deleted');
    }
}
