<?php

use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/products', function () {
    return view('index');
})->name('products.index');

Route::get('/create', function () {
   return view('create');
})->name('products.create-view');

Route::get('/product-list', [ProductController::class, 'index']);
Route::post('/product-create', [ProductController::class, 'store'])->name('products.create');
Route::get('/product-edit/{product}', [ProductController::class, 'edit'])->name('products.edit');
Route::put('/product-update/{product}', [ProductController::class, 'update'])->name('products.update');
Route::delete('/product-delete/{product}', [ProductController::class, 'destroy'])->name('products.delete');
